#include "Store.h"

#include <sstream>
#include <algorithm>
#include <fstream>

void Store::add_product(std::string const& line) {
    std::stringstream ss(line);
    Product product;
    ss >> product;
    m_products.push_back(product);
}

void Store::write_by_producer(std::string const& producer) const {
    std::vector<Product> result;
    std::copy_if(
        m_products.begin(),
        m_products.end(),
        std::back_inserter(result),
        [&producer](auto const& product) {
            return product.producer == producer;
        });
    write(result, producer);
}

void Store::write_by_name(std::string const& name) const {
    std::vector<Product> result;
    std::copy_if(
        m_products.begin(),
        m_products.end(),
        std::back_inserter(result),
        [&name](auto const& product) {
            return product.name == name;
        });
    write(result, name);
}

void Store::write(std::vector<Product> const& products, std::string const& name) const {
    std::string path = "../results/" + name + ".txt";
    std::ofstream ofs(path);
    for (auto const& product: products) {
        ofs << product.name << ','
            << product.price << ','
            << product.producer << ','
            << product.weight << '\n';
    }
}

std::ostream& operator<<(
            std::ostream& os,
            std::vector<Product> const& store) {
    for (auto const& product: store) {
        os << product.name << ' ' 
           << product.price << ' '
           << product.producer << ' '
           << product.weight << std::endl;
    }
    return os;
}
