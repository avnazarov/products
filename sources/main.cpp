#include <fstream>

#include "Store.h"

int main() {
    Store store;

    for (size_t i = 1; i < 6; ++i) {
        std::string path = "../producers/producer" + std::to_string(i) + ".txt";
        std::ifstream stream(path);
        if (!stream) {
            throw std::invalid_argument("File not found");
        }

        std::string line;
        while(stream >> line) {
            store.add_product(line);
        }
    }

    store.write_by_producer("Producer1");
    store.write_by_producer("Producer2");
    store.write_by_name("apple");
    
    return 0;
}