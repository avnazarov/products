#pragma once

#include <vector>

#include "Product.h"

class Store {
public:
    void add_product(std::string const& line);
    void write_by_producer(std::string const& producer) const;
    void write_by_name(std::string const& name) const;
private:
    void write(std::vector<Product> const& products, std::string const& name) const;
    std::vector<Product> m_products;
};

std::ostream& operator<<(
            std::ostream& os,
            std::vector<Product> const& store);