#pragma once

#include <string>
#include <iostream>

struct Product {
    std::string name;
    float price;
    std::string producer;
    float weight;
};

inline std::istream& operator>>(std::istream& is, Product& product) {
    std::getline(is, product.name, ',');
    is >> product.price;
    is.ignore(1);
    std::getline(is, product.producer, ',');
    is >> product.weight;
    return is;
}
